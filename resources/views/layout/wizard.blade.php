<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>
        Wizard
    </title>
    <link href="{{ mix('/css/styles.css') }}" rel="stylesheet">
</head>
<body>
<div id="app"></div>
<script src="{{ mix('/js/index.js') }}"></script>
</body>
</html>
