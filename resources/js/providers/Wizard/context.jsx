import { createContext } from 'react';

const WizardContext = createContext({
    categories: null,
    setChecklistDataByIndexes: null,
    getChecklistDataByIndexes: null
});

export default WizardContext;