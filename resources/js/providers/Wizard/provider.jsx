import React, { useCallback, useState } from 'react';
import WizardContext from './context.jsx';
import * as PropTypes from 'prop-types';
import categories from '../../config/categories.js';

const WizardProvider = ({ children }) => {
    const [checklistData, setChecklistData] = useState(() => {
        const newChecklistData = [];

        categories.forEach((c, ci) => {
            c.subCategories.forEach((sc, sci) => {
                sc.checklist.forEach((ch, chi) => {
                    newChecklistData.push({
                        categoryIndex: ci,
                        subCategoryIndex: sci,
                        checklistIndex: chi,
                        photos: [],
                        state: null
                    });
                })
            });
        });

        return newChecklistData;
    });

    const getChecklistDataByIndexes = useCallback((ci, sci, chi) => {
        const index = checklistData.findIndex(d => d.categoryIndex === ci && d.subCategoryIndex === sci && d.checklistIndex === chi);

        if (index === -1) {
            return null;
        }

        return checklistData[index];
    }, [checklistData]);

    const setChecklistDataByIndexes = useCallback((ci, sci, chi, newChecklistData) => {
        setChecklistData(prevState => {
            const newState = [...prevState];
            const index = newState.findIndex(d => d.categoryIndex === ci && d.subCategoryIndex === sci && d.checklistIndex === chi);

            if (index === -1) {
                return newState;
            }

            newState[index] = newChecklistData;

            return newState;
        });
    }, []);

    return (
        <WizardContext.Provider value={{
            categories,
            checklistData,
            setChecklistDataByIndexes,
            getChecklistDataByIndexes
        }}>
            {children}
        </WizardContext.Provider>
    );
};

WizardProvider.propTypes = {
    children: PropTypes.node.isRequired
};

export default WizardProvider;