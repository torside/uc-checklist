import 'bootstrap';
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import Wizard from './components/Wizard/index.jsx';
import WizardProvider from './providers/Wizard/provider.jsx'

if (document.getElementById('app')) {
    ReactDOM.render((
        <BrowserRouter>
            <WizardProvider>
                <Wizard />
            </WizardProvider>
        </BrowserRouter>
    ), document.getElementById('app'));
}