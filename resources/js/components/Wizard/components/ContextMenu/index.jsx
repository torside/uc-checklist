import React, { useState } from 'react';
import { FloatingMenu, MainButton, ChildButton } from 'react-floating-button-menu';
import MdAdd from '@material-ui/icons/add';
import MdClose from '@material-ui/icons/clear';
import MdSave from '@material-ui/icons/save';
import MdShare from '@material-ui/icons/share';
import MdAddAPhoto from '@material-ui/icons/addAPhoto';
import MdEmail from '@material-ui/icons/email';
import MdChat from '@material-ui/icons/chat';

const ContextMenu = () => {
    const [isOpen, setIsOpen] = useState(false);

    return (
        <div className="context-menu">
            <FloatingMenu slideSpeed={500} direction="left" spacing={8} isOpen={isOpen}>
                <MainButton
                    iconResting={<MdAdd style={{ fontSize: 20 }} />}
                    iconActive={<MdClose style={{ fontSize: 20 }} />}
                    onClick={() => setIsOpen(!isOpen)}
                    size={56} />
                <ChildButton icon={<MdSave style={{ fontSize: 20 }} />} size={40} />
                <ChildButton icon={<MdShare style={{ fontSize: 20 }} />} size={40} />
                <ChildButton icon={<MdAddAPhoto style={{ fontSize: 20 }} />} size={40} />
                <ChildButton icon={<MdEmail style={{ fontSize: 20 }} />} size={40} />
                <ChildButton icon={<MdChat style={{ fontSize: 20 }} />} size={40} />
            </FloatingMenu>
        </div>
    );
};

export default ContextMenu;