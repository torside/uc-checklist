import React, { useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

const ScrollToTop = props => {
    const {
        history,
        children
    } = props;

    useEffect(() => {
        const callback = history.listen(() => {
            window.scrollTo(0, 0);
        });

        return () => {
            callback();
        }
    }, [history]);

    return (
        <>
            {children}
        </>
    );
};

ScrollToTop.propTypes = {
    history: PropTypes.object.isRequired,
    children: PropTypes.node.isRequired
};

export default withRouter(ScrollToTop);