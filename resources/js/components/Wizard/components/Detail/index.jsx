import React from 'react';
import Body from './components/Body/index.jsx';
import BottomPanel from './components/BottomPanel/index.jsx';

const Detail = () => {
    return (
        <div className="detail">
            <Body />
            <BottomPanel />
        </div>
    );
};

export default Detail;