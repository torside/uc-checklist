import React, { useContext } from 'react';
import { useParams } from 'react-router-dom';
import WizardContext from './../../../../../../providers/Wizard/context.jsx';

const Body = () => {
    const {
        categoryIndex,
        subCategoryIndex,
        checklistIndex
    } = useParams();

    const {
        categories,
    } = useContext(WizardContext);

    const data = categories[parseInt(categoryIndex)].subCategories[parseInt(subCategoryIndex)].checklist[parseInt(checklistIndex)];

    let imagesComponent = null;

    if (data.images) {
        imagesComponent = (
            <div className="row">
                <div className="col-12">
                    {data.images.map(i => (
                        <img
                            key={i}
                            src={'/images/advices/' + categoryIndex + '-' + subCategoryIndex + '-' + checklistIndex + '/' + i}
                            alt={data.label}
                            style={{ width: '100%', borderRadius: '4px' }} />
                    ))}
                </div>
            </div>
        );
    }

    return (
        <div className="body">
            <div className="row">
                <div className="col-12 text-center">
                    <h2>
                        {data.label}
                    </h2>
                </div>
            </div>
            <div className="row my-4">
                <div className="col-12 text-center">
                    <div className="body__icon--idea" />
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    {data.advices.map(a => <p key={Math.random()} className="lead">{a}</p>)}
                </div>
            </div>
            {imagesComponent && (imagesComponent)}
        </div>
    );
};

export default Body;