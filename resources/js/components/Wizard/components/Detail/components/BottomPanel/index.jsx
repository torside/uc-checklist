import React, { useContext, useCallback } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import WizardContext from './../../../../../../providers/Wizard/context.jsx';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft, faArrowRight, faCheck, faTimes } from '@fortawesome/free-solid-svg-icons';

const BottomPanel = () => {
    const history = useHistory();

    const {
        categoryIndex,
        subCategoryIndex,
        checklistIndex
    } = useParams();

    const {
        categories,
        setChecklistDataByIndexes,
        getChecklistDataByIndexes
    } = useContext(WizardContext);

    const currentChecklistData = getChecklistDataByIndexes(parseInt(categoryIndex), parseInt(subCategoryIndex), parseInt(checklistIndex));

    const checkOpacity = currentChecklistData.state === true ? '1' : '.4';
    const timesOpacity = currentChecklistData.state === false ? '1' : '.4';

    const navigationPrev = useCallback(() => {
        let newCategoryIndex = parseInt(categoryIndex);
        let newSubCategoryIndex = parseInt(subCategoryIndex);
        let newChecklistIndex = parseInt(checklistIndex) - 1;

        if (newChecklistIndex < 0) {
            newSubCategoryIndex = newSubCategoryIndex - 1;

            if (newSubCategoryIndex < 0) {
                newCategoryIndex = newCategoryIndex - 1;

                if (newCategoryIndex < 0) {
                    newCategoryIndex = categories.length - 1;
                }

                newSubCategoryIndex = categories[newCategoryIndex].subCategories.length - 1;
            }

            newChecklistIndex = categories[newCategoryIndex].subCategories[newSubCategoryIndex].checklist.length - 1;
        }

        history.push('/detail/' + newCategoryIndex + '/' + newSubCategoryIndex + '/' + newChecklistIndex);
    }, [categoryIndex, subCategoryIndex, checklistIndex, categories, history]);

    const navigationNext = useCallback(() => {
        if (typeof categories[parseInt(categoryIndex)].subCategories[parseInt(subCategoryIndex)].checklist[parseInt(checklistIndex) + 1] !== 'undefined') {
            const url = '/detail/' + parseInt(categoryIndex) + '/' + parseInt(subCategoryIndex) + '/' + (parseInt(checklistIndex) + 1);
            history.push(url);

            return;
        }

        if (typeof categories[parseInt(categoryIndex)].subCategories[parseInt(subCategoryIndex) + 1] !== 'undefined') {
            const url = '/detail/' + parseInt(categoryIndex) + '/' + (parseInt(subCategoryIndex) + 1) + '/0';
            history.push(url);

            return;
        }

        if (typeof categories[parseInt(categoryIndex) + 1] !== 'undefined') {
            const url = '/detail/' + (parseInt(categoryIndex) + 1) + '/0/0';
            history.push(url);

            return;
        }

        const url = '/detail/0/0/0';
        history.push(url);
    }, [categoryIndex, subCategoryIndex, checklistIndex, categories, history]);

    return (
        <div className="bottom-panel">
            <button
                className="bottom-panel__button"
                onClick={navigationPrev}>
                <FontAwesomeIcon icon={faArrowLeft} />
            </button>
            <button
                className="bottom-panel__button d-none"
                onClick={() => {
                    currentChecklistData.state = true;

                    setChecklistDataByIndexes(parseInt(categoryIndex), parseInt(subCategoryIndex), parseInt(checklistIndex), currentChecklistData);
                }}
                style={{ opacity: checkOpacity, color: '#18C500' }}>
                <FontAwesomeIcon icon={faCheck} />
            </button>
            <button
                className="bottom-panel__button d-none"
                onClick={() => {
                    currentChecklistData.state = false;

                    setChecklistDataByIndexes(parseInt(categoryIndex), parseInt(subCategoryIndex), parseInt(checklistIndex), currentChecklistData);
                }}
                style={{ opacity: timesOpacity, color: '#FF0000' }}>
                <FontAwesomeIcon icon={faTimes} />
            </button>
            <button
                className="bottom-panel__button"
                onClick={navigationNext}>
                <FontAwesomeIcon icon={faArrowRight} />
            </button>
        </div>
    );
};

export default BottomPanel;