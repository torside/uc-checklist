import React, { useContext } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import SubCategoryPropType from './../../../../../../prop-type/sub-category.js';
import * as classnames from 'classnames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDoubleRight } from '@fortawesome/free-solid-svg-icons';
import WizardContext from '../../../../../../providers/Wizard/context.jsx';

const SubCategory = props => {
    const history = useHistory();

    const {
        categoryIndex
    } = useParams();

    const {
        checklistData
    } = useContext(WizardContext);

    const {
        subCategoryData,
        subCategoryIndex
    } = props;

    const hasFail = checklistData.findIndex(c => c.state === false && c.categoryIndex === parseInt(categoryIndex) && c.subCategoryIndex === subCategoryIndex) > -1;

    const isSuccess = checklistData.findIndex(c => c.state !== true && c.categoryIndex === parseInt(categoryIndex) && c.subCategoryIndex === subCategoryIndex) === -1;

    return (
        <div
            className={classnames(
                'category-list__sub-category',
                { 'category-list__sub-category--has-fail': hasFail },
                { 'category-list__sub-category--is-success': isSuccess }
            )}
            onClick={() => history.push('/categories/' + categoryIndex + '/' + subCategoryIndex)}>
            <div
                className="sub-category-item__icon"
                style={{ backgroundImage: 'url(/images/icons/categories/border-coloured/' + categoryIndex + '-' + subCategoryIndex + '.png)' }} />
            <div className="sub-category-item__body">
                <div className="sub-category-item__label">
                    {subCategoryData.label}
                </div>
                <div className="sub-category-item__more">
                    <FontAwesomeIcon icon={faAngleDoubleRight} size="1x" />
                </div>
            </div>
        </div>
    );
};

SubCategory.propTypes = {
    subCategoryData: SubCategoryPropType.isRequired,
    subCategoryIndex: PropTypes.number.isRequired
};

export default SubCategory;