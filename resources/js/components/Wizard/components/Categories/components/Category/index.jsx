import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import CategoryPropType from './../../../../../../prop-type/category.js';
import * as classnames from 'classnames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDoubleRight } from '@fortawesome/free-solid-svg-icons';
import WizardContext from '../../../../../../providers/Wizard/context.jsx';

const Category = props => {
    const history = useHistory();

    const {
        checklistData
    } = useContext(WizardContext);

    const {
        categoryData,
        categoryIndex
    } = props;

    const hasFail = checklistData.findIndex(c => c.state === false && c.categoryIndex === categoryIndex) > -1;

    const isSuccess = checklistData.findIndex(c => c.state !== true && c.categoryIndex === categoryIndex) === -1;

    return (
        <div
            className={classnames(
                'category-list__category',
                { 'category-list__category--has-fail': hasFail },
                { 'category-list__category--is-success': isSuccess },
            )}
            onClick={() => history.push('/categories/' + categoryIndex)}>
            <div
                className="category__icon"
                style={{ backgroundImage: 'url(/images/icons/categories/border-coloured/' + categoryIndex + '.png)' }} />
            <div className="category__label">
                {categoryData.label}
            </div>
            <div className="category__notes">
                {categoryData.notes.map(n => <span key={n} className="notes_note">- {n}</span>)}
            </div>
            <div className="category__more">
                <FontAwesomeIcon icon={faAngleDoubleRight} size="2x" />
            </div>
        </div>
    );
};

Category.propTypes = {
    categoryData: CategoryPropType.isRequired,
    categoryIndex: PropTypes.number.isRequired
};

export default Category;