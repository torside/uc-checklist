import React, { useContext } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import ChecklistItemPropType from './../../../../../../prop-type/checklist.js';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDoubleRight, faCheck, faTimes } from '@fortawesome/free-solid-svg-icons';
import WizardContext from '../../../../../../providers/Wizard/context.jsx';

const ChecklistItem = props => {
    const history = useHistory();

    const {
        categoryIndex,
        subCategoryIndex
    } = useParams();

    const {
        setChecklistDataByIndexes,
        getChecklistDataByIndexes
    } = useContext(WizardContext);

    const {
        checklistData,
        checklistIndex
    } = props;

    const currentChecklistData = getChecklistDataByIndexes(parseInt(categoryIndex), parseInt(subCategoryIndex), parseInt(checklistIndex));

    const checkOpacity = currentChecklistData.state === true ? '1' : '.4';
    const timesOpacity = currentChecklistData.state === false ? '1' : '.4';

    return (
        <div className="category-list__checklist-item">
            <div className="checklist-item__icons">
                <button
                    className="checklist-item__icon"
                    onClick={() => {
                        currentChecklistData.state = true;

                        setChecklistDataByIndexes(parseInt(categoryIndex), parseInt(subCategoryIndex), parseInt(checklistIndex), currentChecklistData);
                    }}
                    style={{
                        opacity: checkOpacity,
                        color: '#18C500',
                        boxShadow: currentChecklistData.state === true ? '0 0 15px 1px #18C500' : ''
                    }}>
                    <FontAwesomeIcon icon={faCheck} size="lg" />
                </button>
                <button
                    className="checklist-item__icon"
                    onClick={() => {
                        currentChecklistData.state = false;

                        setChecklistDataByIndexes(parseInt(categoryIndex), parseInt(subCategoryIndex), parseInt(checklistIndex), currentChecklistData);
                    }}
                    style={{
                        opacity: timesOpacity,
                        color: '#FF0000',
                        boxShadow: currentChecklistData.state === false ? '0 0 15px 1px #FF0000' : ''
                    }}>
                    <FontAwesomeIcon icon={faTimes} size="lg" />
                </button>
            </div>
            <div
                className="checklist-item__body"
                onClick={() => history.push('/detail/' + categoryIndex + '/' + subCategoryIndex + '/' + checklistIndex)}>
                <div className="checklist-item__label">
                    {checklistData.label}
                </div>
                <div className="checklist-item__more">
                    <FontAwesomeIcon icon={faAngleDoubleRight} size="1x" />
                </div>
            </div>
        </div>
    );
};

ChecklistItem.propTypes = {
    checklistData: ChecklistItemPropType.isRequired,
    checklistIndex: PropTypes.number.isRequired
};

export default ChecklistItem;