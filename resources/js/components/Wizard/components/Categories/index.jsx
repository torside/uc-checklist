import React, { useContext } from 'react';
import WizardContext from './../../../../providers/Wizard/context.jsx';
import { useParams } from 'react-router-dom';
import Category from './components/Category/index.jsx';
import SubCategory from './components/SubCategory/index.jsx';
import ChecklistItem from './components/ChecklistItem/index.jsx';

const Categories = () => {
    const {
        categoryIndex,
        subCategoryIndex
    } = useParams();

    const {
        categories
    } = useContext(WizardContext);

    if (subCategoryIndex) {
        return (
            <div className="categories">
                <div className="categories__category-list">
                    {(categories[categoryIndex].subCategories[subCategoryIndex].checklist.map((checklistData, i) => (
                        <ChecklistItem key={checklistData.id} checklistData={checklistData} checklistIndex={i} />
                    )))}
                </div>
            </div>
        );
    }

    if (categoryIndex) {
        return (
            <div className="categories">
                <div className="categories__category-list">
                    {(categories[categoryIndex].subCategories.map((subCategoryData, i) => (
                        <SubCategory key={subCategoryData.id} subCategoryData={subCategoryData} subCategoryIndex={i} />
                    )))}
                </div>
            </div>
        );
    }

    return (
        <div className="categories">
            <div className="categories__category-list">
                {(categories.map((categoryData, i) => (
                    <Category key={categoryData.id} categoryData={categoryData} categoryIndex={i} />
                )))}
            </div>
        </div>
    );
};

export default Categories;