import React, { useContext } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';
import WizardContext from './../../../../providers/Wizard/context.jsx';

const Breadcrumbs = () => {
    const history = useHistory();

    const {
        categoryIndex,
        subCategoryIndex,
        checklistIndex
    } = useParams();

    const {
        categories
    } = useContext(WizardContext);

    return (
        <div className="breadcrumbs">
            <>
                <button
                    onClick={() => history.push('/categories')}>
                    Domov
                </button>
            </>
            {categoryIndex && (
                <>
                    <FontAwesomeIcon icon={faChevronRight} />
                    <button
                        onClick={() => history.push('/categories/' + categoryIndex)}>
                        {categories[categoryIndex].label}
                    </button>
                </>
            )}

            {subCategoryIndex && (
                <>
                    <FontAwesomeIcon icon={faChevronRight} />
                    <button
                        onClick={() => history.push('/categories/' + categoryIndex + '/' + subCategoryIndex)}>
                        {categories[categoryIndex].subCategories[subCategoryIndex].label}
                    </button>
                </>
            )}

            {checklistIndex && (
                <>
                    <FontAwesomeIcon icon={faChevronRight} />
                    <button
                        onClick={() => history.push('/detail/' + categoryIndex + '/' + subCategoryIndex + '/' + checklistIndex)}>
                        {categories[categoryIndex].subCategories[subCategoryIndex].checklist[checklistIndex].label}
                    </button>
                </>
            )}
        </div>
    );
};

export default Breadcrumbs;