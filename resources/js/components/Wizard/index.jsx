import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Breadcrumbs from './components/Breadcrumbs/index.jsx';
import Categories from './components/Categories/index.jsx';
import Detail from './components/Detail/index.jsx';
import ScrollToTop from './components/ScrollToTop/index.jsx';
//import ContextMenu from './components/ContextMenu/index.jsx';

const Wizard = () => {
    return (
        <div className="wizard">
            <div className="content-wrapper">
                <ScrollToTop>
                    <Switch>
                        <Route path="/categories/:categoryIndex?/:subCategoryIndex?/:checklistIndex?">
                            <Breadcrumbs />
                            <Categories />
                        </Route>
                        <Route path="/detail/:categoryIndex/:subCategoryIndex/:checklistIndex">
                            <Breadcrumbs />
                            <Detail />
                        </Route>
                        <Route render={() => <Redirect to="/categories" />} />
                    </Switch>
                </ScrollToTop>
            </div>
        </div>
    );
};

export default Wizard;