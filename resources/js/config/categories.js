const categories = [
    {
        id: 0,
        label: 'Exteriér',
        subCategories: [
            {
                id: 0,
                label: 'Karoséria',
                checklist: [
                    {
                        id: 0,
                        label: 'Pomarančový lak',
                        advices: [
                            'Dôležitá je drsnosť alebo hrbolatosť povrchu. Lak by mal byť hladký. Nerovnomernosť laku na karosérii môže znamenať striekaný diel.'
                        ],
                        images: [
                            'image-01.jpg'
                        ]
                    },
                    {
                        id: 1,
                        label: 'Rovnomernosť špár',
                        advices: [
                            'Všetky „aktívne“ časti by mali lícovať so zvyškom karosérie. Skontrolujte časti medzi blatníkmi a kapotou, okolo svetiel, medzi dverami a okolo piatych dverí (kufra). Akákoľvek asymetria značí ich opravu po nehode či neodbornú výmenu za nový kus.'
                        ],
                        images: [
                            'image-01.jpg'
                        ]
                    },
                    {
                        id: 2,
                        label: 'Korózia',
                        advices: [
                            'Okraje dverí, blatníky a spodné časti karosérie sú vodou najexponovanejšie, často sa práve v týchto miestach objaví korózia.'
                        ],
                        images: [
                            'image-01.jpg'
                        ]
                    },
                    {
                        id: 3,
                        label: 'Zvary',
                        advices: [
                            'Nepravidelnosť zvaru alebo jeho zjavná nedokonalsoť môže byť indikátorom opráv či dokonca skladania z iných áut.'
                        ]
                    },
                    {
                        id: 4,
                        label: 'Strecha',
                        advices: [
                            'Škrabance na streche môžu signalizovať, že auto slúžilo ako taxík alebo vozidlo autoškoly. Pri skladaní mechanizmu označenia často dochádza aj k poškodeniu strechy. Kontrola je dôležitá aj z dôvodu prípadných priehlbín od krupobytia, kedže strecha je menej viditeľné miesto hlavne pri vyšších vozidlách.'
                        ]
                    },
                    {
                        id: 5,
                        label: 'Podblatníky',
                        advices: [
                            'Ľahko skontrolovateľná časť, podblatníky musia sedieť, bez výrazných poškodení spôsobených demontážou.'
                        ]
                    },
                    {
                        id: 6,
                        label: 'Pánty',
                        advices: [
                            'Poškodený lak na hlave skrutky držiacej pánty dverí, kapoty alebo predných blatníkov býva viditeľným pozostatkom demontáže dielu. Dodatočné prelakovanie sa častokrát nerieši.'
                        ]
                    },
                    {
                        id: 7,
                        label: 'Kufor',
                        advices: [
                            'Poškodenie spodnej strany kufra, kde je uložené rezerva je náročné na opravu aj lakovanie. Toto miesto nie je priamo viditeľné a preto býva ignorované pri opravách.  Navyše prítopmnosť blata, piesku alebo vody môže znamenať vytopenie vozidla.'
                        ]
                    },
                    {
                        id: 8,
                        label: 'VIN',
                        advices: [
                            'Ide o najdôležitejšie (rodné) číslo automobilu. Čísla musia byť vyrazené bez poškodenia alebo viditeľnej manipulácie s dielom, na ktorom sú. VIN musí súhlasiť s údajmi v technickom preukaze. Kód VIN nájdete väčšinou vyrazený v motorovom priestore napr. na kryte tlmiču, tzv. klobúku, na prepážke medzi motorom a kabínou, na prahoch pod tapacírom alebo v batožinovom priestore. Najviditeľnejšie býva umiestnený v spodnej časti čelného skla, ale nemusí to byť pravidlom.'
                        ],
                        images: [
                            'image-01.jpg'
                        ]
                    }
                ]
            },
            {
                id: 1,
                label: 'Kolesá',
                checklist: [
                    {
                        id: 0,
                        label: 'Hĺbka dezénu pneumatiky',
                        advices: [
                            'Príliš nízky dezén bude znamenať nutnú výmenu pneumatík, v SR platí pre zimné plášte minimálna hĺbka dezénu 3 mm a pre letné 1,6 mm.'
                        ]
                    },
                    {
                        id: 1,
                        label: 'Rok výroby pneumatiky',
                        advices: [
                            'Určuje ho DOT kód na bočnici pneumatiky. Najdôležitejšia je tretia časť, kde sú štyri číslice väčšinou orámované oválom. Prvé dve číslice označujú kalendárny týždeň, druhé dve rok. Kód  (0215) znamená, že pneumatika bola vyrobená v druhom kalendárnom týždni roka 2015. Spravidla sa odporúča meniť už 4 ročné pneumatiky.'
                        ]
                    },
                    {
                        id: 2,
                        label: 'Rovnomernosť zjazdenia',
                        advices: [
                            'Ak zistíte nerovnomerne zjazdený behúň pneumatiky môže to byť dôsledkom nesprávnej geometrie vozidla.'
                        ]
                    },
                    {
                        id: 3,
                        label: 'Veľkosť disku podľa TP',
                        advices: [
                            'Niektorí predajcovia neumýselne, niekedy aj zámerne zamenia disky s pneumatikami za iný väčší rozmer, ktorý ale nie je povolený resp. zapísaný v technickom preukaze. Tento fakt vyvolá dodatočný náklad na zaobstaranie si správneho obutia.'
                        ]
                    },
                    {
                        id: 4,
                        label: 'Brzdy',
                        advices: [
                            'Ak to prevedenie diskov umožňuje pozrite sa na stav brzdových platničiek a kotúčov, okrem hrúbky obloženia veľa napovie aj hrdza alebo viditeľný zlý stav.'
                        ]
                    }
                ]
            },
            {
                id: 2,
                label: 'Sklá',
                checklist: [
                    {
                        id: 0,
                        label: 'Praskliny na čelnom skle',
                        advices: [
                            'Skontrolujte či sa na čelnom skle nenachádza väčšie poškodenie s nábehom praskliny. Takéto poškodenie môže v budúcnosti vyžadovať výmenu celého čelného skla.'
                        ]
                    },
                    {
                        id: 1,
                        label: 'Rok výroby skiel',
                        advices: [
                            'Posledné číslo vyleptané napr. na bočných sklách hovorí o roku výroby. Číslo 9 znamená, že vozidlo mohlo byť vyrobené v roku 2009 alebo 2019 Pokiaľ má jedno zo skiel toto číslo iné, pravdepodobne došlo k jeho výmene pri prasknutí alebo nehode. V takomto prípade sa na danú časť zamerajte.'
                        ],
                        images: [
                            'image-01.jpg'
                        ]
                    }
                ]
            },
            {
                id: 3,
                label: 'Motorový priestor',
                checklist: [
                    {
                        id: 0,
                        label: 'Olej v motorovom priestore',
                        advices: [
                            'Olej sa najčastejšie zachytí na kryte pod motorom, jeho príliš veľké množstvo môže byť zlým znamením a treba dôkladnejšie preveriť jeho pôvod.'
                        ]
                    },
                    {
                        id: 1,
                        label: 'Prevádzkové kvapaliny',
                        advices: [
                            'Zamerajte sa či sú prevádzkové kvapaliny doplnené v dostatočnom množstve, rovnako môže niečo viac prezradiť aj ich sfarbenie. Napr. špinavá a mastná chladiaca kvapalina môže znamenať problém s tesnosťou hlavy valcov.'
                        ]
                    }
                ]
            }
        ],
        notes: [
            'umyté auto ukáže viac',
            'denné svetlo je viac ako lampa'
        ]
    },
    {
        id: 1,
        label: 'Interiér',
        subCategories: [
            {
                id: 0,
                label: 'Volant + riadiaca páka',
                checklist: [
                    {
                        id: 0,
                        label: 'Opotrebenie',
                        advices: [
                            'Prílišné ošúchanie alebo iné poškodenie musí byť adektvátne deklarovnému nájazdu km.'
                        ]
                    }
                ]
            },
            {
                id: 1,
                label: 'Airbagy',
                checklist: [
                    {
                        id: 0,
                        label: 'Kryt airbagu volantu',
                        advices: [
                            'V prípade, že auto bolo búrané správne spasovanie krytu býva častý problém, napovie o tom nezlýcovanie krytu so zvyškom volantu.'
                        ]
                    },
                    {
                        id: 1,
                        label: 'Poškodenie stropnice',
                        advices: [
                            'Po vystrelení stropných airbagov dochádza k poškodeniu - zalomeniu tapacíru stropu.'
                        ]
                    },
                    {
                        id: 2,
                        label: 'Prešitie sedačiek',
                        advices: [
                            'Pri kontrole sedačiek si všímajte aj prešitie bočných častí, po vystrelení bočných airbagov dochádza k ich pretrhnutiu, spoj v týchto miestach je na to určený, neodborná oprava by mohla znamenať absenciu airbagu alebo jeho nefunkčnosť.'
                        ]
                    }
                ]
            },
            {
                id: 2,
                label: 'Sedačky',
                checklist: [
                    {
                        id: 0,
                        label: 'Miera opotrebenia',
                        advices: [
                            'Deformácia a ošúchanie ľavej bedrovej a sedacej časti predovšetkým sedačky vodiča musí byť adekvátne kilometrovému nájazdu vozidla.'
                        ]
                    }
                ]
            },
            {
                id: 3,
                label: 'Elektronika',
                checklist: [
                    {
                        id: 0,
                        label: 'Rádio / navigácia',
                        advices: [
                            'Ak sa nezaplo samo, zapnite rádio, vyskúšajte pridávanie hlasitosti, častou chybou býva problém v otočných ovládačoch s potenciometrom, ktorý by sa prejavil šušťavým zvukom.'
                        ]
                    },
                    {
                        id: 1,
                        label: 'Kontrolky',
                        advices: [
                            'Strčte kľúč do zapaľovania a otočte ho do prvej polohy (ready). Rozsvietiť by sa mali všetky kontrolky, ak nezhasnú postupne, ale všetky naraz, môže to byť zlý signál.'
                        ]
                    },
                    {
                        id: 2,
                        label: 'Ovládanie všetkých okien',
                        advices: [
                            'Buďte dôsledný, sadnite si na každé miesto vo vozidle a vyskúšajte ovládanie všetkých okien.'
                        ]
                    },
                    {
                        id: 3,
                        label: 'Centrálne zamykanie',
                        advices: [
                            'Okrem štandardného otvorenia a zatvorenia vozidla vyskúšajte aj komfortné prvky ak sú k dispozícii a to automatické doťahovanie okien alebo vzdialené otvorenie kufra.'
                        ]
                    },
                    {
                        id: 4,
                        label: 'Nezávislé kúrenie',
                        advices: [
                            'V prípade, že ho vozidlo obsahuje určite ho vyskšajte vrátane vzdialeného štartu pomocou ovládača. Jeho oprava by mohla byť výrazným nákladom.'
                        ]
                    },
                    {
                        id: 5,
                        label: 'Výhrev sedačiek',
                        advices: []
                    },
                    {
                        id: 6,
                        label: 'Nastavenia sedačiek',
                        advices: [
                            'Ak má vozidlo pamäťové sedačky vyskúšajte aj nastavenie do správnej polohy po otovrení vozidla cez centrál.'
                        ]
                    },
                    {
                        id: 7,
                        label: 'Funkčnosť klimatizácie',
                        advices: [
                            'Po zapnutí by malo byť počuť zopnutie kompresora, ak nie môže to znamenať chýbajúce chladivo. V prípade, že z klímy ide zápach alebo nechladí bude potrebné ju prečistiť a opätovne naplniť.'
                        ]
                    }
                ]
            },
            {
                id: 4,
                label: 'Priestor pod nohami',
                checklist: [
                    {
                        id: 0,
                        label: 'Vlhkosť pod koberčekmi',
                        advices: [
                            'Vlhoba pod nohami, ak nie je spôsobená v zime snehom z topánok býva následok vadného tesnenia okolo čelného skla alebo upchatím odvodových kanálikov. Následkom môže byť zápach a hniloba pod tapacírom.'
                        ]
                    }
                ]
            }
        ],
        notes: [
            'nehanbite sa pozrieť všade',
            'opýtať sa či to funguje nestačí'
        ]
    },
    {
        id: 2,
        label: 'Skúšobná jazda',
        subCategories: [
            {
                id: 0,
                label: 'Prevodovka',
                checklist: [
                    {
                        id: 0,
                        label: 'Hladké radenie',
                        advices: [
                            'Pri manuálnych prevodovkách by mala páka hladko zapadať v koncových polohách a pohybovať sa vo svojich dráhach. Nezabudnite vyskúšať aj spiatočku, Automatická prevodovka by mala radiť hladko bez rušivej akustiky alebo trhania. Prevod stupňov by mal byť čo najplynulejší.'
                        ]
                    }
                ]
            },
            {
                id: 1,
                label: 'Výfuk',
                checklist: [
                    {
                        id: 0,
                        label: 'Zvuk a vibrácie',
                        advices: [
                            'V prípade, že počujete zo spodnej časti vozidla klepotavý alebo dunivý zvuk ako so športových výfukov môže sa jednať o uveľnenie výfukového potrebia alebo jeho držiakov. Prípadne o dieru vo výfukovom potrubí.'
                        ]
                    },
                    {
                        id: 1,
                        label: 'Farba dymu',
                        advices: [
                            'Modrý dym - je spôsobený spaľovaním oleja. Dôvodom môže byť netesniace výfukové potrubie , prepúšťajúce tesnenie pod hlavou, zlý typ oleja alebo nadmerným množstvom oleja v motore. Modrý dym tak často odráža zlý stav motora a vysoký počet najazdených kilometrov.',
                            'Biely dym - benzínových motorov je spôsobený najčastejšie vodou v zapaľovanej zmesi. U naftových motorov môže byť dôvodom nezhorené palivo priamo vo výfuku spôsobené vadným systémom vstrekovania',
                            'Čierny (a tmavosivý) dym - môže ísť o vadné turbo, nesprávnu tolerancia ventilov, nízku kompresiu, vadný EGR ventil ...'
                        ]
                    },
                    {
                        id: 2,
                        label: 'Množstvo sadzí',
                        advices: [
                            'Veľmi veľa sadzí vo výfuku môže znamenať nedokonalé spalovanie.'
                        ]
                    }
                ]
            },
            {
                id: 2,
                label: 'Brzdy',
                checklist: [
                    {
                        id: 0,
                        label: 'Účinnosť',
                        advices: [
                            'Pred jazdou vyskúšajte efektivitu ručnej brzdy, ak je to možné urobte to v kopci, či je účinok brzdy dostatočný. Jej voľnosť by mohla byť problémom pri neskoršej technickej kontrole. Rovnako tak vyskúšajte účinok elektronickej ručnej brzdy.'
                        ]
                    }
                ]
            },
            {
                id: 3,
                label: 'Riadenie / jazda',
                checklist: [
                    {
                        id: 0,
                        label: 'Rovnomernosť jazdy',
                        advices: [
                            'Po pustení volantu na rovnej ceste by vozidlo nemalo samovoľne ťahať do strany, môže to byť znakom chybného riadenia alebo geometrie vozdila.'
                        ]
                    },
                    {
                        id: 1,
                        label: 'Praskanie a dunenie v zákrute',
                        advices: [
                            'Veľmi často zreteľné aj pri rozbiehaní s vytočenými kolesami alebo pri jazde na spiatočke. Možnou príčinou je poškodený homokinetický kĺb na hnacom hriadeli.'
                        ]
                    },
                    {
                        id: 2,
                        label: 'Škrípanie pri brzdení',
                        advices: [
                            'Takýmto škrípaním sa môže prejaviť nadmerné opotrebenie brzdového obloženia, spôsobujúce pri brzdení trenie kovu o kov.'
                        ]
                    },
                    {
                        id: 3,
                        label: 'Klepotanie na nerovnostiach',
                        advices: [
                            'Najčastejšie sú problémom silentbloky stabilizátora, uloženie vzpier, alebo dielov zavesenia kolesa. Niekedy môže byť chyba aj v poškodených tlmičoch.'
                        ]
                    },
                    {
                        id: 4,
                        label: 'Otáčky',
                        advices: [
                            'Nechajte bežať vozdilo na voľnobehu a sledujte otáčky, tie by sa mali držať v konštantnej hladine a nemali by kolísať.'
                        ]
                    }
                ]
            },
            {
                id: 4,
                label: 'Tempomat',
                checklist: [
                    {
                        id: 0,
                        label: 'Aktivácia / deaktivácia',
                        advices: [
                            'Ak to jazda umožní vyskúšajte aj funkčnosť tempomatu, jeho zapnutie a udržiavanie rýchlosti. Ide o komfortný prvok, na ktorý sa často pri skúšobných jazdách zabúda. Stačí aj test počas krátkej trasy v meste.'
                        ]
                    }
                ]
            },
            {
                id: 5,
                label: 'Turbo',
                checklist: [
                    {
                        id: 0,
                        label: 'Hlučnosť a záber',
                        advices: [
                            'Ak vozdilo má turbo zamerajte sa na pískajúci zvuk, ak sa objavý pri zvyšovaní otáčok, môže to znamenať pokazenú jedna z turbín. Problém s turbom sa prejavuje aj poklesom výkonu, po zvýšení otáčok (zošliapnutí plynu) nedôjde k efektu výraznejšieho zrýchlenia.'
                        ]
                    },
                    {
                        id: 1,
                        label: 'Farba dymu',
                        advices: [
                            'Problém s turbom signalizuje modrý alebo čierny dym z výfuku, v oboch prípadoch ide o problém pri spaľovaní a môže sa jednať o nákladú opravu.'
                        ]
                    }
                ]
            },
            {
                id: 6,
                label: 'Bezpečnostné pásy',
                checklist: [
                    {
                        id: 0,
                        label: 'Funkčnosť',
                        advices: [
                            'Vyskúšajte či sa pás trhnutím zasekne. Väčšina automobilov už má aj signalizáciu nezapnutých pásov, ktorú je jedoduché preveriť.'
                        ]
                    }
                ]
            }
        ],
        notes: [
            'naštartujte vy',
            'ideálne pri studenom motore',
            'sledujte kontrolky',
            'čím dlhšia tým lepšia,'
        ]
    },
    {
        id: 3,
        label: 'Doklady',
        subCategories: [
            {
                id: 0,
                label: 'Servisná knižka',
                checklist: [
                    {
                        id: 0,
                        label: 'Vierohodnosť',
                        advices: [
                            'Dajte si záležať s skontrolujte záznamy v knižke, slovenské záznamy z autorizovaných servisov je možné overiť veľakrát aj telefonicky. Pri tých zahraničných dávajte pozor aby neboli podozrivo rovnaké, podpisované rôznymi menami ale rovnakým perom. Prípadne ak pri servisných záznamoch pečiatky chýbajú nie je pravdepodobné ,že záznamy budú pravé.'
                        ]
                    }
                ]
            },
            {
                id: 1,
                label: 'Faktúry',
                checklist: [
                    {
                        id: 0,
                        label: 'Bloky za sevis',
                        advices: [
                            'Podľa blokov za nákup náhradných dielov alebo servisné úkony môžete zmapovať periodicitu a rozsah vykonávaných opráv pôvodného majiteľa. V prípade, že sa aspoň trochu orientujete môžete sa zamerať aj na kvalitatívnu stránku vymenených dielov podľa ich značky alebo cenovej hladiny.'
                        ]
                    },
                    {
                        id: 1,
                        label: 'Bloky za služby',
                        advices: [
                            'Rovnako ako bloky za servis Vám napoviedia aj faktúry za služby ako napríklad chiptunnig, rôzne prestavby, detailing atď.'
                        ]
                    }
                ]
            },
            {
                id: 2,
                label: 'Certifikáty',
                checklist: [
                    {
                        id: 0,
                        label: 'Okenné fólie',
                        advices: [
                            'Pri cestnej kontrole ste povinný sa preukázať certifikátom od okenných fólií pokiaľ nie sú namontované na vozidle od výroby, v opačnom prípade je možné, že ich budete musieť pri kontrole odstrániť, rovnako môžete mať problém pri technickej kontrole.'
                        ]
                    },
                    {
                        id: 1,
                        label: 'Ťažné zariadenie',
                        advices: [
                            'Ťažné zariadenie je nutná mať namontované od spoločností na to certifikovaných.'
                        ]
                    },
                    {
                        id: 2,
                        label: 'Zabezpečovacie zariadenie',
                        advices: [
                            'Niektoré poisťovne vyžadujú pri uzatváraní poistky doložiť certifikát resp. typový list od namontovaného zabezpečovacieho zariadenia, môže sa vám preto zísť.'
                        ]
                    },
                    {
                        id: 3,
                        label: 'Prestavba na LPG/CNG',
                        advices: [
                            'Prestava musí byť realizované len na to certifikovanou spoločnosťou.'
                        ]
                    }
                ]
            },
            {
                id: 3,
                label: 'Poistky',
                checklist: [
                    {
                        id: 0,
                        label: 'Zákonná poistka',
                        advices: [
                            'Ide o zákonnú povinnosť, jej absenciu by ste museli riešiť pred jazdou uzatvorením novej.'
                        ]
                    }
                ]
            },
            {
                id: 4,
                label: 'TK a EK',
                checklist: [
                    {
                        id: 0,
                        label: 'Dátum platnosti',
                        advices: [
                            'Dôležitá informácia pre obraz budúcich nákladov, jej vypršanie po kúpe by mohlo vyvolať pokutu.'
                        ]
                    }
                ]
            },
            {
                id: 5,
                label: 'Technický preukaz',
                checklist: [
                    {
                        id: 0,
                        label: 'Majiteľ',
                        advices: [
                            'Skontrolujte či vozidlo predáva osoba, ktorá je aj jeho majiteľom. Skontrolujte či sa náležitosti v technickom preukaze a predovšetkým VIN číslo zhoduje s číslom na karosérii.'
                        ]
                    }
                ]
            }
        ],
        notes: [
            'zamerajte sa na históriu',
            'pri kontrole si dajte záležať'
        ]
    },
    {
        id: 4,
        label: 'Rady starých mám',
        subCategories: [
            {
                id: 0,
                label: 'Na čo nezabudúť',
                checklist: [
                    {
                        id: 0,
                        label: 'Sezónne prezutie',
                        advices: [
                            'Sezónne prezutie veľa predjacov rieši samostatným predajom, nezabudnite sa spýtať, či je k dispozícii. Môže byť lacnejšou alternatívou ako keď ho budete zaobstarávať až následne prípadne môže poslúžiť ako nástroj ak predajca nebude chcieť znížiť cenu.'
                        ]
                    },
                    {
                        id: 1,
                        label: 'Nosič lyží, bicyklov',
                        advices: [
                            'Veľakrát sa na tento kus príslušenstva zabúda, ide o praktický doplnkok, ktorý je zväčša pôvodnému majiteľovi nanič. Buď vám môže nosiš prihidť k vozidlu alebo priamo ponúknuť za priaznivejšiu cenu ako nový.'
                        ]
                    },
                    {
                        id: 2,
                        label: 'Kúpna zmluva',
                        advices: [
                            'Najdôležitejšia vec pri samotnej realizácii kupy. Veľa predajcov má pripravené svoje verzie, dajte pozor aby v zmluve bol popísaný stav vozidla pri preberaní a garancia najazdených km. Dôležitá je aj časť o podmienkach odstúpenia od zmluvy napr. v prípade zistenia stočených km.'
                        ]
                    },
                    {
                        id: 3,
                        label: 'Zjednať cenu',
                        advices: [
                            'Zjednávať, zjednávať, zjednávať ... väčšina predajcov už pri cenotvorbe počíta, že k takémuto pokusu príde a sú ochotní spustiť cenu aj pri vozidle, ktorého stav je adekvátny tomu v inzeráte. V momente keď pri obhliadke identifikujte skryté nedostatky malo by byť zníženie ceny samoszrejmosťou.'
                        ]
                    },
                    {
                        id: 4,
                        label: 'Povinná výbava',
                        advices: [
                            'Často sa na ňu zabúda, predovšetkým u súkromných predajcov nie je štandardom, že si ju pravidelne kontrolujú. Jej absencia alebo exkspirácia môže pre vás znamenať pokutu pri cestenej kontrole alebo v horšom prípade problém pri nehode. Ide pritom tiež o náklad rádovo desiatok eur.'
                        ]
                    },
                    {
                        id: 5,
                        label: 'Kľúč od bezpečnostných šróbov',
                        advices: [
                            'Ak sú na vozidle bezpečnostné šróby, bez špeciálnej na ne určenej hlavy budete mať s výmenou kolesa problém, nezabudne sa na to spýtať.'
                        ]
                    },
                    {
                        id: 6,
                        label: 'Dva kľúče od vozidla, prípadne servisný a kódovaciu kartu',
                        advices: [
                            'Štandard je dostať k vozidlu dva kľúče, v tom lepšom prípade aj so servisným alebo kódovaciou kartou, tie bývajú často dlho odložené v šuflíkoch a pri predávkach sa na ne často zabúda.'
                        ]
                    },
                    {
                        id: 7,
                        label: 'Pin od rádia',
                        advices: [
                            'Ak ho majiteľ má, ušetrí vám nejaké eurá za jeho zisťovanie v prípade potreby, pričom pôvodnému majiteľovi je zbytočný.'
                        ]
                    },
                    {
                        id: 8,
                        label: 'Navigácia - aktualizácia máp',
                        advices: [
                            'Všetko čo dostatne k navigácii a jej možnej budúcej aktualizácii sa zíde, ušetrené peniaze v budúcnosti.'
                        ]
                    },
                    {
                        id: 9,
                        label: 'Manuál obsluhy vozidla',
                        advices: [
                            'Veľa z manuálov je dostupných aj online, zďaleka nie všetky, ťažko sa zháňajú a v autorizovaných servisoch môžu stáť aj desiatky eur, ak takýto manuál obsluhy predajca má určite ho nezabudnite.'
                        ]
                    }
                ]
            }
        ],
        notes: [
            'tipy, triky, kulehy'
        ]
    }
];

export default categories;