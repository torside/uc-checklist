import PropTypes from 'prop-types';

const ChecklistDataItem = PropTypes.shape({
    categoryIndex: PropTypes.number.isRequired,
    subCategoryIndex: PropTypes.number.isRequired,
    checklistIndex: PropTypes.number.isRequired,
    photos: PropTypes.array.isRequired,
    state: PropTypes.bool
});

export default ChecklistDataItem;