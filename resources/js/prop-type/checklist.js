import PropTypes from 'prop-types';

const ChecklistPropType = PropTypes.shape({
    id: PropTypes.number.isRequired,
    label: PropTypes.string.isRequired,
    advices: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
    images: PropTypes.arrayOf(PropTypes.string.isRequired)
});

export default ChecklistPropType;