import PropTypes from 'prop-types';
import ChecklistPropType from './checklist.js'

const SubCategoryPropType = PropTypes.shape({
    id: PropTypes.number.isRequired,
    label: PropTypes.string.isRequired,
    checklist: PropTypes.arrayOf(ChecklistPropType).isRequired
});

export default SubCategoryPropType;