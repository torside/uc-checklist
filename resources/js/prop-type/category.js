import PropTypes from 'prop-types';
import SubCategoryPropType from './sub-category.js'

const CategoryPropType = PropTypes.shape({
    id: PropTypes.number.isRequired,
    label: PropTypes.string.isRequired,
    subCategories: PropTypes.arrayOf(SubCategoryPropType).isRequired,
    notes: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired
});

export default CategoryPropType;