FROM node:10-alpine AS build

WORKDIR /app

COPY . .

RUN npm ci
RUN npm run prod

FROM nginx

COPY --from=build /app/public /usr/share/nginx/html

RUN rm /usr/share/nginx/html/50x.html
RUN rm /usr/share/nginx/html/index.php
RUN rm /usr/share/nginx/html/js/index.js.LICENSE.txt
RUN rm /usr/share/nginx/html/mix-manifest.json
