<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;

final class Build extends Command
{
    /** @var string $signature */
    protected $signature = 'build';

    /**
     *
     */
    public function handle()
    {
        /** @var Filesystem $disk */
        $disk = Storage::disk('local');

        if ($disk->exists('build')) {
            $disk->deleteDirectory('build');
        }

        $disk->makeDirectory('build');

        /** @var string $index */
        $index = str_replace('/js/index.js', 'js/index.js', str_replace('/css/styles.css', 'css/styles.css', view('layout.wizard')));

        $disk->put('build/index.html', $index);


        $this->copyDirectory(public_path('images'), storage_path('app/build/images'));

        $disk->put('build/css/styles.css', file_get_contents(public_path('css/styles.css')));
        $disk->put('build/js/index.js', file_get_contents(public_path('js/index.js')));
    }

    /**
     * @param string $source
     * @param string $destination
     */
    private function copyDirectory(string $source, string $destination)
    {
        /** @var resource $directory */
        $directory = opendir($source);

        @mkdir($destination);

        while (false !== ($file = readdir($directory))) {
            if (($file !== '.') && ($file !== '..')) {
                if (is_dir("{$source}/{$file}")) {
                    $this->copyDirectory("{$source}/{$file}", "{$destination}/{$file}");
                    continue;
                }

                copy("{$source}/{$file}", "{$destination}/{$file}");
            }
        }

        closedir($directory);
    }
}
