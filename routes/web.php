<?php

use Illuminate\Support\Facades\Route;

Route::any('{params}', function () {
    return view('layout.wizard');
})
    ->where('params', '(.*)?');
