const mix = require('laravel-mix');

mix.copyDirectory('resources/images', 'public/images');

mix.react('resources/js/index.jsx', 'public/js')
    .sass('resources/sass/styles.scss', 'public/css');

mix.version();
